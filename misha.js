var successCallback = function(data) {

	var checkout_form = $( 'form.woocommerce-checkout' );

	// agrega un token a nuestro campo de entrada oculto
	// console.log(data) to find the token
	checkout_form.find('#misha_token').val(data.token);

	// desactivar el evento de la función tokenRequest
	checkout_form.off( 'checkout_place_order', tokenRequest );

	// enviar el formulario ahora

	checkout_form.submit();

};

var errorCallback = function(data) {
    console.log(data);
};

var tokenRequest = function() {

	// aquí habrá una función de pasarela de pago que procesará todos los datos de la tarjeta de su formulario,
    // tal vez necesite su clave de API publicable, que es misha_params.publishableKey 
    // y activa SuccessCallback() en caso de éxito y errorCallback en caso de error
	return false;
		
};

jQuery(function($){

	var checkout_form = $( 'form.woocommerce-checkout' );
	checkout_form.on( 'checkout_place_order', tokenRequest );

});
