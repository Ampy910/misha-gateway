<?php
/*
 * Plugin Name: WooCommerce Custom Payment Gateway
 * Plugin URI: https://rudrastyh.com/woocommerce/payment-gateway-plugin.html
 * Description: Take credit card payments on your store.
 * Author: Misha Rudrastyh
 * Author URI: http://rudrastyh.com
 * Version: 1.0.1
 */

/*
 * Este gancho de acción registra nuestra clase de PHP como pasarela de pago de WooCommerce
 */
add_filter( 'woocommerce_payment_gateways', 'misha_add_gateway_class' );
function misha_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Misha_Gateway'; // el nombre de tu clase está aquí
	return $gateways;
}
/* * La clase en sí, tenga en cuenta que está dentro del gancho de acción plugins_loaded */
add_action( 'plugins_loaded', 'misha_init_gateway_class' );
function misha_init_gateway_class() {

	class WC_Misha_Gateway extends WC_Payment_Gateway {

 		/** * Constructor de clases, más sobre esto en el Paso 3 */
 		   public function __construct() {

			$this->id = 'misha'; // ID del complemento de la pasarela de pago
            $this->icon = ''; // URL del ícono que se mostrará en la página de pago cerca del nombre de su puerta de enlace
            $this->has_fields = true; // en caso de que necesite un formulario de tarjeta de crédito personalizado
            $this->method_title = 'Misha Gateway';
            $this->method_description = 'Description of Misha payment gateway'; // en caso de que necesite un formulario de tarjeta de crédito personalizado

           // las puertas de enlace pueden admitir suscripciones, reembolsos, métodos de pago guardados,
           // pero en este tutorial comenzamos con pagos simples
            $this->supports = array(
                'products'
            );

            // Método con todos los campos de opciones
            $this->init_form_fields();

            // Cargar la configuración.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            $this->private_key = $this->testmode ? $this->get_option( 'test_private_key' ) : $this->get_option( 'private_key' );
            $this->publishable_key = $this->testmode ? $this->get_option( 'test_publishable_key' ) : $this->get_option( 'publishable_key' );

            // Este enlace de acción guarda la configuración
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

            // Necesitamos JavaScript personalizado para obtener un token
            add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
            
            // También puede registrar un webhook aquí
            // add_action( 'woocommerce_api_{webhook name}', array( $this, 'webhook' ) );


 		   }

		/** * Opciones de complementos, también lo tratamos en el Paso 3 */
 		   public function init_form_fields(){

			$this->form_fields = array(
                
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Credit Card',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via our super-cool payment gateway.',
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
                ),
                'test_publishable_key' => array(
                    'title'       => 'Test Publishable Key',
                    'type'        => 'text'
                ),
                'test_private_key' => array(
                    'title'       => 'Test Private Key',
                    'type'        => 'password',
                ),
                'publishable_key' => array(
                    'title'       => 'Live Publishable Key',
                    'type'        => 'text'
                ),
                'private_key' => array(
                    'title'       => 'Live Private Key',
                    'type'        => 'password'
                )
            );
	
	 	   }

		/* * CSS y JS personalizados, en la mayoría de los casos solo se requieren cuando decidiste ir con un formulario de tarjeta de crédito personalizado */
	 	   public function payment_scripts() {
            

            // necesitamos JavaScript para procesar un token solo en las páginas de carrito/pago, ¿verdad?
            if ( ! is_cart() && ! is_checkout() && ! isset( $_GET['pay_for_order'] ) ) {
                return;
            }

            // si nuestra pasarela de pago está deshabilitada, no tenemos que poner en cola JS también
            if ( 'no' === $this->enabled ) {
                return;
            }

            // no hay razón para poner en cola JavaScript si las claves API no están configuradas
            if ( empty( $this->private_key ) || empty( $this->publishable_key ) ) {
                return;
            }

            // no trabaje con detalles de tarjetas sin SSL a menos que su sitio web esté en modo de prueba
            if ( ! $this->testmode && ! is_ssl() ) {
                return;
            }

            // supongamos que es nuestro procesador de pago JavaScript el que permite obtener un token
            wp_enqueue_script( 'misha_js', './misha.js' );

            // y este es nuestro JS personalizado en su directorio de complementos que funciona con token.js
            wp_register_script( 'woocommerce_misha', plugins_url( 'misha.js', __FILE__ ), array( 'jquery', 'misha_js' ) );

            // en la mayoría de los procesadores de pago tienes que usar CLAVE PÚBLICA para obtener un token
            wp_localize_script( 'woocommerce_misha', 'misha_params', array(
                'publishableKey' => $this->publishable_key
            ) );

            wp_enqueue_script( 'woocommerce_misha' );

            
	    }

		/* * Validación de campos*/
		public function payment_fields() {
            
            // ok, mostremos una descripción antes del formulario de pago
            if ( $this->description ) {
                // puede obtener instrucciones para el modo de prueba, me refiero a los números de tarjeta de prueba, etc.
                if ( $this->testmode ) {
                    $this->description .= ' TEST MODE ENABLED. In test mode, you can use the card numbers listed in <a href="#">documentation</a>.';
                    $this->description  = trim( $this->description );
                }
                //mostrar la descripción con etiquetas <p>, etc.
                echo wpautop( wp_kses_post( $this->description ) );
            }
            ?>
            <!--  Repetiré () el formulario, pero puede cerrar las etiquetas de PHP e imprimirlo directamente en HTML -->
            <fieldset id="wc-' . esc_attr( $this->id ) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent; padding-bottom:15px;">
            <?php
            // Agregue este enlace de acción si desea que su pasarela de pago personalizada lo admita
            do_action( 'woocommerce_credit_card_form_start', $this->id );

            // I recommend to use inique IDs, because other gateways could already use #ccNo, #expdate, #cvc
            ?>
            
            <div class="form-row form-row-wide"><label class="card">Card Number <span class="required">*</span></label>
                <input id="misha_ccNo" type="text" autocomplete="off">
                </div>
                <div class="form-row form-row-first">
                    <label class="card">Expiry Date <span class="required">*</span></label>
                    <input id="misha_expdate"  type="text" autocomplete="off" placeholder="MM / YY">
                </div>
                <div class="form-row form-row-last">
                    <label class="card">Card Code (CVC) <span class="required">*</span></label>
                    <input class="" id="misha_cvv" type="password" autocomplete="off" placeholder="CVC">
                </div>
                <div class="clear"></div>
            <?php
            do_action( 'woocommerce_credit_card_form_end', $this->id );

            echo '<div class="clear"></div></fieldset>';

		}

		/* * Estamos procesando los pagos aquí*/



		public function process_payment( $order_id ) {

            if( empty( $_POST[ 'billing_first_name' ]) ) {
                wc_add_notice(  'First name is required!', 'error' );
                return false;
            }
            return true;
	    }



		/* * En caso de que necesite un webhook, como PayPal IPN, etc. */
		public function webhook() {
            $order = wc_get_order( $_GET['id'] );
            $order->payment_complete();
            $order->reduce_order_stock();

            update_option('webhook_debug', $_GET);

            global $woocommerce;

            // lo necesitamos para obtener los detalles del pedido
            $order = wc_get_order( $order_id );

            /* * Matriz con parámetros para interacción API */

            $args = array();
        
            /* * Su interacción API podría construirse con wp_remote_post() */
            $response = wp_remote_post( '{payment processor endpoint}', $args );
                
            if( !is_wp_error( $response ) ) {
        
                $body = json_decode( $response['body'], true );
        
                 // podría ser diferente dependiendo de su procesador de pagos
                if ( $body['response']['responseCode'] == 'APPROVED' ) {
        
                    // recibimos el pago
                    $order->payment_complete();
                    $order->reduce_order_stock();
        
                    // algunas notas para el cliente (reemplazar verdadero con falso para hacerlo privado)
                    $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
        
                    // Carro vacio
                    $woocommerce->cart->empty_cart();
        
                    // Redirigir a la página de agradecimiento
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url( $order )
                    );
        
                } else {
                    wc_add_notice(  'Please try again.', 'error' );
                    return;
                }
        
            } else {
                wc_add_notice(  'Connection error.', 'error' );
                return;
            }
        
	    }
    }
}

add_action( 'wp_enqueue_scripts', 'misha_init_gateway_class_css' );
function misha_init_gateway_class_css() {
 
    wp_register_style( 'misha_init_gateway_class_css', plugin_dir_url(__FILE__).'/style.css?v=2' );
    wp_enqueue_style( 'misha_init_gateway_class_css' );
 
}















?>  

            